package models;

import Util.DateTime;
import io.ebean.annotation.DbArray;

import javax.persistence.Entity;
import java.util.Arrays;
import java.util.List;

@Entity
public class Dvd extends LibraryItem {


    @DbArray
    private List<String> languages;

    @DbArray
    private List<String> subtitles;

    @DbArray
    private List<String> cast;

    public Dvd(String isbn, String title, String sector, DateTime publishedDate, DateTime dateTime, Reader reader,
               String languages, String subtitles, String cast) {
        super(isbn, title, sector, publishedDate, dateTime, reader);
        setLanguages(languages);
        setCast(cast);
        setSubtitles(subtitles);
    }

    public Dvd(String isbn, String title, String sector, DateTime publishedDate,
               String languages, String subtitles, String cast) {
        super(isbn, title, sector, publishedDate);
        setLanguages(languages);
        setCast(cast);
        setSubtitles(subtitles);
    }

    public String getLanguages() {
        return languages.toString();
    }

    public void setLanguages(String languages) {
        this.languages = Arrays.asList(languages.split(","));
    }

    public String getSubtitles() {
        return subtitles.toString();
    }

    public void setSubtitles(String subtitles) {
        this.subtitles = Arrays.asList(subtitles.split(","));
    }

    public String getCast() {
        return cast.toString();
    }

    public void setCast(String cast) {
        this.cast = Arrays.asList(cast.split(","));
    }
}
