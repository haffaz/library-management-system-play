package models;

import Util.DateTime;
import io.ebean.annotation.DbArray;

import javax.persistence.Entity;
import java.util.Arrays;
import java.util.List;

@Entity
public class Book extends LibraryItem {

    private String publisher;
    private int pages;

    @DbArray
    private List<String> authors;

    public Book(String isbn, String title, String sector, DateTime publishedDate, DateTime dateTime, Reader reader,
                String authors, String publisher, int pages) {
        super(isbn, title, sector, publishedDate, dateTime, reader);
        setAuthors(authors);
        this.publisher = publisher;
        this.pages = pages;
    }

    public Book(String isbn, String title, String sector, DateTime publishedDate, String authors, String publisher, int pages) {
        super(isbn, title, sector, publishedDate);
        setAuthors(authors);
        this.publisher = publisher;
        this.pages = pages;
    }

    public String getAuthors() {
        return authors.toString();
    }

    public void setAuthors(String authors) {

        this.authors = Arrays.asList(authors.split(","));
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }
}
