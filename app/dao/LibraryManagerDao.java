package dao;

import com.google.inject.ImplementedBy;
import exceptions.IdNotFoundException;
import exceptions.InvalidReaderException;
import exceptions.IsbnAlreadyInUseException;
import models.LibraryItem;
import models.Reader;

import java.util.List;

/**
 * LibraryManagerDao is the interface containing the functions of the library management system.
 * This serves in the data access layer
 */
@ImplementedBy(LibraryManagerDaoImpl.class)
public interface LibraryManagerDao {

    String addLibraryItem(LibraryItem libraryItem) throws IsbnAlreadyInUseException;

    String deleteLibraryItem(String isbn) throws IdNotFoundException;

    LibraryItem getLibraryItem(String isbn) throws IdNotFoundException;

    List<LibraryItem> getAllLibraryItems();

    LibraryItem borrowLibraryItem(Reader reader, String burrowedDateTime, String isbn) throws IdNotFoundException;

    LibraryItem returnLibraryItem(String isbn) throws IdNotFoundException;

    String addReader(Reader reader) throws Exception;

    String deleteReader(String id) throws InvalidReaderException;

    Reader getReader(String id) throws InvalidReaderException;

    List<Reader> getAllReaders() throws Exception;
}
