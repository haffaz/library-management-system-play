package dao;

import exceptions.IdNotFoundException;
import exceptions.InvalidReaderException;
import exceptions.IsbnAlreadyInUseException;
import models.LibraryItem;
import models.Reader;

import java.util.ArrayList;
import java.util.List;

public class LibraryManagerDaoImpl implements LibraryManagerDao {

    private static List<LibraryItem> libraryItemList;
    private static List<Reader> readerList;

    public LibraryManagerDaoImpl() {
        libraryItemList = new ArrayList<>();
        readerList = new ArrayList<>();
    }

    @Override
    public String addLibraryItem(LibraryItem libraryItem) throws IsbnAlreadyInUseException {

        List<LibraryItem> libraryItems = getAllLibraryItems();

        for (LibraryItem item : libraryItems) {
            if (libraryItem.getIsbn().equals(item.getIsbn())) {
                throw new IsbnAlreadyInUseException(libraryItem.getIsbn());
            }

        }
        libraryItemList.add(libraryItem);
        return libraryItem.getIsbn();
    }

    private boolean isReaderValid(String id) throws InvalidReaderException {
        for (Reader reader : readerList) {
            if (id == reader.getId()) {
                return true;
            }
        }
        throw new InvalidReaderException(String.valueOf(id));
    }

    @Override
    public String deleteLibraryItem(String isbn) throws IdNotFoundException {
        LibraryItem libraryItem = findLibraryItem(isbn);
        libraryItemList.remove(libraryItem);
        return isbn;
    }

    @Override
    public LibraryItem getLibraryItem(String isbn) throws IdNotFoundException {
        List<LibraryItem> libraryItems = getAllLibraryItems();
        for (LibraryItem item : libraryItems) {
            if (item.getIsbn().equals(isbn)) {
                return item;
            }
        }
        throw new IdNotFoundException(isbn);
    }

    @Override
    public List<LibraryItem> getAllLibraryItems() {
        return libraryItemList;
    }

    @Override
    public LibraryItem borrowLibraryItem(Reader reader, String burrowedDateTime, String isbn) throws IdNotFoundException {
        LibraryItem libraryItem = findLibraryItem(isbn);
        libraryItem.setReader(reader);
        libraryItem.setDateTime(burrowedDateTime);
        return libraryItem;
    }

    @Override
    public LibraryItem returnLibraryItem(String isbn) throws IdNotFoundException {
        LibraryItem libraryItem = findLibraryItem(isbn);
        libraryItem.setReader(null);
        libraryItem.setDateTime(null);
        return libraryItem;
    }

    @Override
    public String addReader(Reader reader) throws Exception {
        readerList.add(reader);
        return reader.getName();
    }

    @Override
    public String deleteReader(String id) throws InvalidReaderException {
        String type = getReader(id).getClass().getTypeName();
        readerList.remove(id);
        return type;
    }

    @Override
    public Reader getReader(String id) throws InvalidReaderException {
        for (Reader reader : readerList) {
            if (reader.getId().equals(id)) {
                return reader;
            }
        }
        throw new InvalidReaderException(id.toString());
    }

    @Override
    public List<Reader> getAllReaders() throws Exception {
        return readerList;
    }

    private LibraryItem findLibraryItem(String isbn) throws IdNotFoundException {
        for (LibraryItem libraryItem : libraryItemList) {
            if (libraryItem.getIsbn().equals(isbn)) {
                return libraryItem;
            }
        }
        throw new IdNotFoundException(isbn);
    }

}