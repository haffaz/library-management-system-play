package exceptions;

public class InvalidReaderException extends Exception {

    public InvalidReaderException(String id) {
        super("Reader Id [" + id + "] not found in database");
    }

}
