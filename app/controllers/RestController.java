package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import exceptions.IsbnAlreadyInUseException;
import models.Book;
import models.Dvd;
import models.LibraryItem;
import models.Reader;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import response.Util;
import services.LibraryManager;

import java.util.List;

public class RestController extends Controller {

    @Inject
    LibraryManager libraryManager;

    public Result addBook() {
        JsonNode json = request().body().asJson();
        if (json == null) {
            return badRequest(Util.createResponse(
                    "Expecting Json data", false
            ));
        }

        try {
            Book book = (Book) Json.fromJson(json, Book.class);
            libraryManager.addLibraryItem((LibraryItem) book);
            JsonNode jsonObject = Json.toJson(book);
            return created(Util.createResponse(jsonObject, true));
        } catch (IsbnAlreadyInUseException e) {
            return badRequest(Util.createResponse("Id already exist.", false
            ));
        }

    }

    public Result addDvd() {
        JsonNode json = request().body().asJson();
        if (json == null) {
            return badRequest(Util.createResponse("Expecting Json data", false
            ));
        }

        try {
            Dvd dvd = (Dvd) Json.fromJson(json, Dvd.class);
            libraryManager.addLibraryItem((LibraryItem) dvd);
            JsonNode jsonObject = Json.toJson(dvd);
            return created(Util.createResponse(jsonObject, true));
        } catch (IsbnAlreadyInUseException e) {
            return badRequest(Util.createResponse("Id already exist.", false
            ));
        }
    }

    public Result delete(String isbn) {
        try {
            String type = libraryManager.deleteLibraryItem(isbn);
            return created(Util.createResponse(type, true));
        } catch (Exception e) {
            return badRequest(Util.createResponse(e.getMessage(), false
            ));
        }
    }


    //    public Result addDvd() {
//        JsonNode json = request().body().asJson();
//        if (json == null) {
//            return badRequest(Util.createResponse("Expecting Json data", false
//            ));
//        }
//
//        try {
//            Dvd dvd = (Dvd) Json.fromJson(json, Dvd.class);
//            libraryManager.addLibraryItem((LibraryItem) dvd);
//            JsonNode jsonObject = Json.toJson(dvd);
//            return created(Util.createResponse(jsonObject, true));
//        } catch (IsbnAlreadyInUseException e) {
//            return badRequest(Util.createResponse("Id already exist.", false
//            ));
//        }
//    }
    public Result returnItem(String isbn) {
        try {
            LibraryItem libraryItem = libraryManager.getLibraryItem(isbn);
            libraryManager.returnLibraryItem(libraryItem.getIsbn());
            JsonNode jsonObject = Json.toJson(libraryItem);
            return created(Util.createResponse(jsonObject, true));
        } catch (Exception e) {
            return badRequest(Util.createResponse(e.getMessage(), false
            ));
        }
    }

    public Result burrow(String isbn, String burrowedDateTime) {
        try {
            JsonNode json = request().body().asJson();
            LibraryItem libraryItem = libraryManager.getLibraryItem(isbn);
            Reader reader = (Reader) Json.fromJson(json, Reader.class);
            libraryManager.borrowLibraryItem(reader, burrowedDateTime, isbn);
            JsonNode jsonObject = Json.toJson(libraryItem);
            return created(Util.createResponse(jsonObject, true));
        } catch (Exception e) {
            return badRequest(Util.createResponse(e.getMessage(), false
            ));
        }
    }

    public Result displayItems() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode jsonNode = Json.toJson(libraryManager.getLibraryItems());
            return created(Util.createResponse(jsonNode, true
            ));
        } catch (Exception e) {
            return badRequest(Util.createResponse(e.getMessage(), false));
        }
    }

    public Result getReport() {
        List<LibraryItem> libraryItems = libraryManager.generateReport();

        try {
            JsonNode jsonNode = Json.toJson(libraryItems);
            return created(jsonNode);
        } catch (Exception e) {
            return badRequest(e.getMessage());
        }
    }
}