package services;

import dao.LibraryManagerDao;
import exceptions.IdNotFoundException;
import exceptions.InvalidReaderException;
import exceptions.IsbnAlreadyInUseException;
import models.LibraryItem;
import models.Reader;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class WestminsterLibraryManager implements LibraryManager {

    private final LibraryManagerDao libraryManagerDao;

    private List<LibraryItem> browsedHistory;

    @Inject
    public WestminsterLibraryManager(LibraryManagerDao libraryManagerDao) {
        this.libraryManagerDao = libraryManagerDao;
    }

    @Override
    public String addLibraryItem(LibraryItem libraryItem) throws IsbnAlreadyInUseException {
        return libraryManagerDao.addLibraryItem(libraryItem);
    }

    @Override
    public String deleteLibraryItem(String isbn) throws IdNotFoundException {
        return libraryManagerDao.deleteLibraryItem(isbn);
    }

    @Override
    public LibraryItem getLibraryItem(String isbn) throws IdNotFoundException {
        return libraryManagerDao.getLibraryItem(isbn);
    }

    @Override
    public List<LibraryItem> getLibraryItems() {
        return libraryManagerDao.getAllLibraryItems();
    }

    @Override
    public LibraryItem borrowLibraryItem(Reader reader, String burrowedDateTime, String isbn) throws IdNotFoundException {
        return libraryManagerDao.borrowLibraryItem(reader, burrowedDateTime, isbn);
    }

    @Override
    public LibraryItem returnLibraryItem(String isbn) throws IdNotFoundException {
        return libraryManagerDao.returnLibraryItem(isbn);
    }

    @Override
    public String addReader(Reader reader) throws Exception {
        return libraryManagerDao.addReader(reader);
    }

    @Override
    public String deleteReader(String id) throws IdNotFoundException, InvalidReaderException {
        return libraryManagerDao.deleteReader(id);
    }

    @Override
    public Reader getReader(String id) throws IdNotFoundException, InvalidReaderException {
        return libraryManagerDao.getReader(id);
    }

    @Override
    public List<Reader> getAllReaders() throws Exception {
        return libraryManagerDao.getAllReaders();
    }

    @Override
    public List<LibraryItem> generateReport() {
        browsedHistory = new ArrayList<>();
        for (LibraryItem libraryItem : libraryManagerDao.getAllLibraryItems()) {
            if (libraryItem.getReader() != null) {
                browsedHistory.add(libraryItem);
            }
        }
        return browsedHistory;
    }
}
