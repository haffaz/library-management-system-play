package services;

import com.google.inject.ImplementedBy;
import exceptions.IdNotFoundException;
import exceptions.InvalidReaderException;
import exceptions.IsbnAlreadyInUseException;
import models.LibraryItem;
import models.Reader;

import java.util.List;

/**
 * LibraryManager is an interface containing the functions of the LibraryManagement system
 * This interface serves as an service layer interface
 */
@ImplementedBy(WestminsterLibraryManager.class)
public interface LibraryManager {

    String addLibraryItem(LibraryItem libraryItem) throws IsbnAlreadyInUseException;

    String deleteLibraryItem(String isbn) throws Exception;

    LibraryItem getLibraryItem(String isbn) throws IdNotFoundException;

    List<LibraryItem> getLibraryItems() throws Exception;

    LibraryItem borrowLibraryItem(Reader reader, String burrowedDateTime, String isbn) throws IdNotFoundException;

    LibraryItem returnLibraryItem(String isbn) throws IdNotFoundException;

    String addReader(Reader reader) throws Exception;

    String deleteReader(String id) throws IdNotFoundException, InvalidReaderException;

    Reader getReader(String id) throws IdNotFoundException, InvalidReaderException;

    List<Reader> getAllReaders() throws Exception;

    List<LibraryItem> generateReport();
}
